/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class CockooHashing {

    static int maxSize = 10;
    static int ver = 2;
    static int[][] table = new int[ver][maxSize];
    static int[] position = new int[ver];

    static void initTable() {
        for (int j = 0; j < maxSize; j++) {
            for (int i = 0; i < ver; i++) {
                table[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        switch (function) {
            case 1:
                return key % maxSize; //caculate index;
            case 2:
                return (key / maxSize) % maxSize;//caculate index if index != null;;
        }
        return Integer.MIN_VALUE;
    }

    static void place(int key, int tableID, int cnt, int n) {
        if (cnt == n) {
            System.out.printf("%d unpositionitioned\n", key);
            System.out.print("Error");
            return;
        }

        //add keys in table
        for (int i = 0; i < ver; i++) {
            position[i] = hash(i + 1, key);
            if (table[i][position[i]] == key) {
                return;
            }
        }

        //index != null
        if (table[tableID][position[tableID]] != Integer.MIN_VALUE) {
            int dis = table[tableID][position[tableID]];
            table[tableID][position[tableID]] = key;
            place(dis, (tableID + 1) % ver, cnt + 1, n);
        } else {
            table[tableID][position[tableID]] = key;
        }
    }

    static void cuckoo(int keys[], int n) {
        initTable();

        for (int i = 0, cnt = 0; i < n; i++, cnt = 0) {
            place(keys[i], 0, cnt, n);
        }
    }

    static void showTable() {
        for (int i = 0; i < ver; i++, System.out.printf("\n")) {
            for (int j = 0; j < maxSize; j++) {
                if (table[i][j] == Integer.MIN_VALUE) {
                    System.out.printf("- ");
                } else {
                    System.out.printf("%d ", table[i][j]);
                }
            }
        }
    }

    public static void main(String[] args) {
        int keys_1[] = {10, 20, 30, 40, 55, 53, 20, 56, 39, 3};
        int n = keys_1.length;
        cuckoo(keys_1, n);

        showTable();
    }
}
